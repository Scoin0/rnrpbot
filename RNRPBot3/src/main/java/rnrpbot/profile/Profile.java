package rnrpbot.profile;

import com.j256.ormlite.table.DatabaseTable;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@DatabaseTable (tableName = "profiles")
public class Profile {

    private long userID;
    private String username;
    private List<Long> guildIDs = new ArrayList<>(); // For sorting later
    private String title; //Buyable Titles set up by each Guild Owner
    private long rnrpTokens; //Bot's Currency
    private long totalEXP;
    private long level;
}
