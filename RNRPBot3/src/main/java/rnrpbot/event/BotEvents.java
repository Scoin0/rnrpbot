package rnrpbot.event;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.core.DiscordBot;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.database.controllers.CAccounts;
import rnrpbot.database.controllers.CGuildSettings;
import rnrpbot.database.controllers.GuildController;
import rnrpbot.database.objects.Accounts;
import rnrpbot.database.objects.GuildSettings;
import rnrpbot.utils.LevelingUtils;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

public class BotEvents extends ListenerAdapter {

    static final Logger log = LoggerFactory.getLogger("BotEvents");

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        Guild guild = event.getGuild();
        User owner = guild.getOwner().getUser();
        GuildSettings gs = DatabaseUtils.guifromId(guild.getId());

        if(gs == null) {

            gs = new GuildSettings();

            gs.setGuildID(guild.getId());
            gs.setGuildName(guild.getName());
            gs.setOwnerName(owner.getId());
            gs.setGuildIcon(guild.getIconUrl());
            gs.setCreatedOn(Timestamp.from(Instant.now()));

            TextChannel outChannel = null;

            for (TextChannel channel : guild.getTextChannels()) {
                if(channel.canTalk()) {
                    outChannel = channel;
                    break;
                }
            }

            try {
                DatabaseManager.guildSettingsDao.createOrUpdate(gs);
            } catch (SQLException e) {
                log.warn(e.getMessage());
            }

        }


    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        log.info("Left Guild: <" + event.getGuild().getId() + "> " + event.getGuild().getName());
        GuildSettings gs = DatabaseUtils.guifromId(event.getGuild().getId());
        DatabaseUtils.deleteByGuildId(gs.getGuildID());
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        // Don't listen to bots
        if (event.getAuthor().isBot() || event.getAuthor().isFake()) return;

        // Obtain the users DiscordID
        Accounts accounts = DatabaseUtils.accfromId(event.getAuthor().getId());

        // If the account is not in the system, then create a new account
        if (accounts == null) {

            accounts = CAccounts.createProfile(event.getAuthor().getId(), event.getMember().getUser().getName(), event.getAuthor().getAvatarId());

            try {
                DatabaseManager.accountDao.createOrUpdate(accounts);
            } catch (SQLException e) {
                log.error("Error: " + e.getMessage());
            }
        } else {
            // If already created, give experience
            long randomEXP = LevelingUtils.randomXp(10, 25);
            log.info ("Giving " + randomEXP + "xp to " + accounts.getUsername());
            log.info ("Total xp is now: " + accounts.getTotalXp());
            accounts.setTotalXp(accounts.getTotalXp() + randomEXP);
            CAccounts.save(accounts);
        }
    }
}
