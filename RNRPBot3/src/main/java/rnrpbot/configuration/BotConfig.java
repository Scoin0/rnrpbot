package rnrpbot.configuration;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;

public class BotConfig {

    private static Logger log = LoggerFactory.getLogger("BotConfig");
    private static File file = new File("config.properties");

    //Bot Tokens
    private static String botToken = "";
    private static String giphyToken = "";
    private static String osuToken = "";

    //Database
    private static String DB_User = "";
    private static String DB_Pass = "";
    private static String DB_Name = "";
    private static String DB_Host = "";
    private static String DB_Port = "";

    //Bot Settings
    private static boolean useUpdater = true;

    public static String getBotToken() {
        return botToken;
    }

    public static String getGiphyToken() {
        return giphyToken;
    }

    public static String getOsuToken() {
        return osuToken;
    }

    public static String getDB_User() {
        return DB_User;
    }

    public static String getDB_Pass() {
        return DB_Pass;
    }

    public static String getDB_Name() {
        return DB_Name;
    }

    public static String getDB_Host() {
        return DB_Host;
    }

    public static String getDB_Port() {
        return DB_Port;
    }

    public static boolean getUseUpdater() { return useUpdater; }


    public static void createConfiguration() throws Exception {

        PropertiesConfiguration config = new PropertiesConfiguration(file);

        try {
            if (!file.exists()) {
                log.info("The Config file has not been found. Creating one now...");
                file.createNewFile();

                config.setHeader("RNRPBot Configuration File");

                config.setProperty("bot-token", botToken);
                config.getLayout().setComment("bot-token", "Discord Bot API Token");
                config.setProperty("giphy-token", giphyToken);
                config.getLayout().setComment("giphy-token", "Giphy API Token");
                config.setProperty("osu-token", osuToken);
                config.getLayout().setComment("osu-token", "OSU API Token");

                config.setProperty("db-user", DB_User);
                config.getLayout().setComment("db-user", "Database Username");
                config.setProperty("db-pass", DB_Pass);
                config.getLayout().setComment("db-pass", "Database Password");
                config.setProperty("db-name", DB_Name);
                config.getLayout().setComment("db-name", "Database Name");
                config.setProperty("db-host", DB_Host);
                config.getLayout().setComment("db-host", "Database Host");
                config.setProperty("db-port", DB_Port);
                config.getLayout().setComment("db-port", "Database Port");

                config.setProperty("use-updater", useUpdater);
                config.getLayout().setComment("use-updater", "Use built in updater? (Default = true)");

                config.save();
            }

        } catch (Exception e) {
            log.warn(e.getMessage());
        }

        log.info("Please set up your configuration file.");
        System.exit(0);

    }

    public static void loadConfiguration() throws Exception {

        PropertiesConfiguration config = new PropertiesConfiguration(file);
        FileInputStream fileInput = new FileInputStream(file);
        config.load(fileInput);
        fileInput.close();

        log.info("Loading Configurations...");

        //Bot Tokens
        botToken = config.getString("bot-token");
        giphyToken = config.getString("giphy-token");
        osuToken = config.getString("osu-token");

        //Database
        DB_User = config.getString("db-user");
        DB_Pass = config.getString("db-pass");
        DB_Name = config.getString("db-name");
        DB_Host = config.getString("db-host");
        DB_Port = config.getString("db-port");

        //Bot Settings
        useUpdater = config.getBoolean("use-updater");


        if (botToken.isEmpty()) {
            //log.warn("The bot token must be filled out in order to use this bot! Please enter your API key in the config file!");
            System.exit(0);
        }


        if (DB_User.isEmpty() || DB_Pass.isEmpty() || DB_Name.isEmpty() || DB_Host.isEmpty() || DB_Port.isEmpty()) {
            //log.warn("This bot uses a Database to remember certain events and user data. Please set one up and enter the correct information to these fields!");
            System.exit(0);
        }

       log.info("Configurations loaded.");
    }

    public static void initConfiguration() throws Exception {

        if(file.exists()) {
            loadConfiguration();
        } else {
            createConfiguration();
        }

    }
}
