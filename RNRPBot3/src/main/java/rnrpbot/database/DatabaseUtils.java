package rnrpbot.database;

import rnrpbot.database.objects.Accounts;
import rnrpbot.database.objects.GuildSettings;

public class DatabaseUtils {

    public static Accounts accfromId(String discordID) {

        try {

            Accounts accounts = DatabaseManager.accountDao.queryForEq("discordID", discordID).get(0);

            return accounts;
        } catch (Exception e) {
            if(e instanceof IndexOutOfBoundsException){
                return null;
            }


            return null;

        }
    }

    public static GuildSettings guifromId(String guildID) {

        try {

            GuildSettings guildSettings = DatabaseManager.guildSettingsDao.queryForEq("guildID", guildID).get(0);

            return guildSettings;
        } catch (Exception e) {
            if(e instanceof IndexOutOfBoundsException){
                return null;
            }


            return null;

        }
    }

    public static GuildSettings guifromId(Long guildID) {

        try {

            GuildSettings guildSettings = DatabaseManager.guildSettingsDao.queryForEq("guildID", guildID).get(0);

            return guildSettings;
        } catch (Exception e) {
            if(e instanceof IndexOutOfBoundsException){
                return null;
            }


            return null;

        }
    }

    public static GuildSettings deleteByGuildId(String discordID) {
        try {
            GuildSettings guildSettings = DatabaseManager.guildSettingsDao.queryForEq("guildID", discordID).get(0);
            DatabaseManager.guildSettingsDao.delete(guildSettings);
            return guildSettings;
        } catch (Exception e) {
            if(e instanceof IndexOutOfBoundsException){
                return null;
            }
            return null;
        }
    }

    public static Accounts deleteById(String discordID) {
        try {
            Accounts accounts = DatabaseManager.accountDao.queryForEq("discordID", discordID).get(0);

            DatabaseManager.accountDao.deleteById(Integer.toString(accounts.getId()));
            return accounts;
        } catch (Exception e){
            if(e instanceof IndexOutOfBoundsException){
                return null;
            }
            return null;
        }
    }

}
