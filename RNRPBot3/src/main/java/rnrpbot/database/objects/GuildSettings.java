package rnrpbot.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import net.dv8tion.jda.core.entities.Guild;
import rnrpbot.main.RNRPBot;

import java.sql.Timestamp;

@DatabaseTable(tableName = "guildsettings")
public class GuildSettings {

    //Guild Data
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField (canBeNull = false)
    private String guildID;
    @DatabaseField
    private String guildName;
    @DatabaseField
    private String ownerName;
    @DatabaseField
    private String guildIcon = "";
    @DatabaseField
    private Timestamp createdOn;

    //Guild Settings
    @DatabaseField
    private long botChannel = 0;
    @DatabaseField
    private String prefix = "!";
    @DatabaseField
    private String welcomeMessage = "Welcome to the server, %username%!";
    @DatabaseField
    private long joinChannel = 0;

    //Global
    @DatabaseField
    private boolean banned = false;

    public GuildSettings() {

    }

    public int getId() {
        return id;
    }

    public void setGuildID(String guildID) {
        this.guildID = guildID;
    }

    public String getGuildID() {
        return guildID;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getGuildName() {
        return guildName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerName() {
        Guild guild = RNRPBot.jda.getGuildById(getGuildID());
        setOwnerName(guild.getOwner().getNickname());
        return ownerName;
    }

    public void setGuildIcon(String guildIcon) {
        this.guildIcon = guildIcon;
    }

    public String getGuildIcon() {
        return guildIcon;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setBotChannel(long botChannel) {
        this.botChannel = botChannel;
    }

    public long getBotChannel() {
        return botChannel;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setJoinChannel(long joinChannel) {
        this.joinChannel = joinChannel;
    }

    public long getJoinChannel() {
        return joinChannel;
    }

    public void isBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean getIsBanned() {
        return banned;
    }
}
