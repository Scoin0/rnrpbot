package rnrpbot.database.objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.database.DatabaseManager;

public class AccountController {

    static Logger log = LoggerFactory.getLogger("AccountController");

    public static Accounts fromId(String discordID) {

        try {

            Accounts accounts = DatabaseManager.accountDao.queryForEq("discordID", discordID).get(0);

            return accounts;
        } catch (Exception e) {

            if(e instanceof IndexOutOfBoundsException){
                return null;
            }

            log.warn("Could not get the account information for: " + discordID);
            e.printStackTrace();
            return null;

        }
    }

}
