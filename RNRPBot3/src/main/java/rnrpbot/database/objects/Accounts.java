package rnrpbot.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import net.dv8tion.jda.core.entities.User;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.main.RNRPBot;
import rnrpbot.utils.LevelingUtils;

import java.sql.Timestamp;

@DatabaseTable (tableName = "accounts")
public class Accounts {

    //Account Personal Data
    @DatabaseField (generatedId = true)
    private int id;
    @DatabaseField (canBeNull = false)
    private String discordID;
    @DatabaseField (canBeNull = false)
    private String username;
    @DatabaseField
    private String avatarID;

    //Account Global Data
    @DatabaseField (canBeNull = false)
    private long totalXp = 0;
    @DatabaseField (canBeNull = false)
    private int level = 1;
    @DatabaseField (canBeNull = false)
    private long balance = 0;
    @DatabaseField (canBeNull = false)
    private Timestamp createdOn;

    public Accounts() {

    }

    public int getId() {
        return id;
    }

    public void setDiscordID(String discordID) {
        this.discordID = discordID;
    }

    public String getDiscordID() {
        return discordID;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setAvatarID(String avatarID) {
        this.avatarID = avatarID;
    }

    public String getAvatarID() {
        return avatarID;
    }

    public void setTotalXp(long totalXp) {
        this.totalXp = totalXp;
        setLevel(LevelingUtils.xpToLevels(totalXp));
    }

    public long getTotalXp() {
        return totalXp;
    }

    public void setLevel(int level) {
        if (level > this.level) {
            try {
                DatabaseManager.accountDao.update(this);
                Accounts accounts = DatabaseUtils.accfromId(getDiscordID());
                //RNRPBot.jda.getTextChannelById(gs.getBotChannel()).sendMessage("Hey, " + user.getName() + "! You leveled up! Congrats! You are now level " + (accounts.getLevel() + 1)).complete();
                System.out.println("Hey, " + accounts.getUsername() + "! You leveled up! Congrats! You are now level " + (accounts.getLevel() + 1));
            } catch (Exception e) {

            }

        }

        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public long getBalance() {
        return balance;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }
}
