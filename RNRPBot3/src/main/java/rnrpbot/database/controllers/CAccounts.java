package rnrpbot.database.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.objects.Accounts;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CAccounts {

    static Logger log = LoggerFactory.getLogger("Account Controller");
    static Accounts accounts;

    //TODO: Add Cache

    public static Accounts findByDiscordId(String discordId) {

        try {
            accounts = DatabaseManager.accountDao.queryForEq("discordID", discordId).get(0);
            return accounts;
        } catch (SQLException e) {
            log.warn("Could not find the account for " + discordId);
            e.printStackTrace();
            return null;
        }
    }

    public static Accounts findByDiscordId(long discordId) {

        try {
            accounts = DatabaseManager.accountDao.queryForEq("discordID", discordId).get(0);
            return accounts;
        } catch (SQLException e) {
            log.warn("Could not find the account for " + discordId);
            e.printStackTrace();
            return null;
        }
    }

    public static Accounts createProfile(String discordId, String username, String avatarId) {
        Accounts account = new Accounts();
        account.setDiscordID(discordId);
        account.setUsername(username);
        account.setAvatarID(avatarId);
        account.setCreatedOn(Timestamp.from(Instant.now()));
        log.info("Created the account for " + account.getUsername());
        return account;
    }

    public static Accounts addBannedUserId() {
        //TODO: ADD BANNED USERS
        return null;
    }

    public static List<Accounts> getBannedUsers() {
        //TODO: ADD GET BANNED USERS
        return null;
    }

    public static void save(Accounts account) {
        try {
            DatabaseManager.accountDao.update(account);
        } catch (SQLException e) {
            log.error("Error saving account data. " + e.getMessage());
        }
    }

    public static void delete(Accounts account) {
        try {
            DatabaseManager.accountDao.delete(account);
        } catch (SQLException e) {
            log.error("Error saving account data. " + e.getMessage());
        }
    }
}
