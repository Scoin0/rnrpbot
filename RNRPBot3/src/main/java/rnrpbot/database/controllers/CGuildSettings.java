package rnrpbot.database.controllers;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.objects.GuildSettings;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CGuildSettings {

    static Logger log = LoggerFactory.getLogger("Guild Controller");


    /*  TODO: ADD GUILD CACHE
     *  TODO: GET ALL BANNED GUILDS
     *  TODO: FIND BY GUILDS (STRING, LONG)
     *  TODO: SAVE
     *  TODO: DELETE
     *  TODO: UPDATE
     *  TODO: CREATE NEW
     *  TODO: PLEASE HELP I'M TIRED
     */


    // TODO: Finish getting banned guilds
    public static List<GuildSettings> getBannedGuilds() {
        List<GuildSettings> list = new ArrayList<>();

        try {
            for (GuildSettings guilds: DatabaseManager.guildSettingsDao.queryForAll()) {
                if (guilds.getIsBanned() == true) {
                    list.add(guilds);
                }
            }
        } catch (SQLException e) {
            log.warn("Could not retrieve ban list!" + e.getMessage());
        }
        return list;
    }
}
