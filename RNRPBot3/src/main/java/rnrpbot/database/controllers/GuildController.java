package rnrpbot.database.controllers;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.TextChannel;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.database.objects.GuildSettings;

import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GuildController {

    private static Map<Long, Integer> guildIdCache = new ConcurrentHashMap<>();
    private static Map<Integer, Long> discordIdCache = new ConcurrentHashMap<>();

    public static int getCachedId (MessageChannel channel) throws SQLException {
        if (channel instanceof TextChannel) {
            return getCachedId(((TextChannel) channel).getGuild().getIdLong());
        }
        return 0;
    }

    public static int getCachedId(long discordId) throws SQLException {
        if (!guildIdCache.containsKey(discordId)) {
            DatabaseUtils.guifromId(discordId);
            //Addserver
            GuildSettings guildSettings = DatabaseManager.guildSettingsDao.queryForEq("id", discordId).get(0);
            guildIdCache.put(discordId, guildSettings.getId());
        }
        return guildIdCache.get(discordId);
    }

    public static int getActiveGuildCount() {
        int amount = 0;
        try {
            amount = (int) DatabaseManager.guildSettingsDao.countOf();
        } catch (SQLException e){
            e.getErrorCode();
        }
        return amount;
    }

}
