package rnrpbot.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.configuration.BotConfig;
import rnrpbot.core.ExitCode;
import rnrpbot.database.objects.Accounts;
import rnrpbot.database.objects.GuildSettings;

import java.io.IOException;
import java.sql.SQLException;

public class DatabaseManager {

    static Logger log = LoggerFactory.getLogger("DatabaseManager");
    static BotConfig config = new BotConfig();

    public static Dao<Accounts, String> accountDao;
    public static Dao<GuildSettings, String> guildSettingsDao;

    public static ConnectionSource source = null;

    public static void loadDatabase() {
        log.info("Loading Database...");

        try {
            ConnectionSource source = openConnection();

            accountDao = DaoManager.createDao(source, Accounts.class);
            guildSettingsDao = DaoManager.createDao(source, GuildSettings.class);
            createTables();

            log.info("Database Loaded.");
        } catch (Exception e) {
            log.warn("Unable to load Database! Error code: " );
            log.warn(e.getMessage());
        }

    }

    public static ConnectionSource openConnection() {

        if (source == null) {
            try {

                JdbcPooledConnectionSource source = new JdbcPooledConnectionSource("jdbc:mysql://" + config.getDB_Host() + ":" + config.getDB_Port() + "/" + config.getDB_Name(),
                        config.getDB_User(), config.getDB_Pass());
                source.setMaxConnectionAgeMillis(5 * 60 * 1000);
                source.setCheckConnectionsEveryMillis(60 * 1000);
                source.setTestBeforeGet(true);

                DatabaseManager.source = source;

                return source;
            } catch (Exception e) {
                log.warn("Unable to connect to Database!");
                System.exit(ExitCode.SHUTDOWN.getExitCode());
                return null;
            }
        }

        return source;
    }

    public static void closeConnection() {
        log.info("Shutting down the connection to the Database...");
        try {
            source.close();

            source = null;
            log.info("The connection has been closed to the Database.");
        } catch (IOException e) {
            log.warn("Unable to close the connection to the Database!");
            e.printStackTrace();
        }
    }

    public static void createTables() {

        try {

            TableUtils.createTableIfNotExists(source, Accounts.class);
            TableUtils.createTableIfNotExists(source, GuildSettings.class);

        } catch (SQLException e) {
            log.warn("Unable to create tables! Error code: " + e.getErrorCode());
            log.warn(e.getMessage());
        }
    }
}
