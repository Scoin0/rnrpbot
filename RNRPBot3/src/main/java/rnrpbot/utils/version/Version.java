package rnrpbot.utils.version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Version {

    private int majorVersion;
    private int minorVersion;
    private int patchVersion;
    static final Logger log = LoggerFactory.getLogger("Version");

    public Version(int majorVersion, int minorVersion, int patchVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
        this.patchVersion = patchVersion;
    }

    public Version(int majorVersion, int minorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
        this.patchVersion = 0;
    }

    public Version(int majorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = 0;
        this.patchVersion = 0;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    public int getPatchVersion() {
        return patchVersion;
    }

    public static Version fromString(String version) {
        String[] sect = version.split("\\.");
        if (sect.length == 3) {
            return new Version(parseInt(sect[0], 1), parseInt(sect[1], 0), parseInt(sect[2],0));
        } else if (sect.length == 2) {
            return new Version(parseInt(sect[0], 1), parseInt(sect[1], 0));
        } else if (sect.length == 1) {
            return new Version(parseInt(sect[0], 1));
        }
        return new Version(1);
    }

    public boolean compareVersion(Version version) {
        if (version == null || this.getMajorVersion() > version.getMajorVersion()) {
            return true;
        } else if (this.getMajorVersion() == version.getMajorVersion()) {
            if (this.getMinorVersion() > version.getMinorVersion()) {
                return true;
            } else if (this.getMinorVersion() == version.getMinorVersion()) {
                if (this.getPatchVersion() > version.getPatchVersion()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int parseInt(String intString, int fallback) {
        try {
            return Integer.parseInt(intString);
        } catch (NumberFormatException e) {
            log.warn("Fallback: " + fallback);
            return fallback;
        }
    }

    @Override
    public String toString() {
        return getMajorVersion() + "." + getMinorVersion() + "." + getPatchVersion();
    }
}
