package rnrpbot.utils.version;

import rnrpbot.core.DiscordBot;

import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class VersionTask {

    public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static void updateScheduler() {
        final Runnable updater = new Runnable() {
            @Override
            public void run() {
                VersionUpdate.checkForUpdate();
                DiscordBot.getJDA().getGuildById("114973686632677380").getTextChannelById("424080153568739358").sendMessage("I'm checking for updates!").queue();
            }
        };

        // Every 12 hours check for a new update
        final ScheduledFuture<?> updaterHandle = scheduler.scheduleAtFixedRate(updater, 1, 12, TimeUnit.HOURS);
        }
    }