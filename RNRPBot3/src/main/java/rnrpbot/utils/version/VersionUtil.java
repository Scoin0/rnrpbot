package rnrpbot.utils.version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dmerkushov.httphelper.HttpHelper;
import ru.dmerkushov.httphelper.HttpHelperException;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionUtil {

    static final Logger log = LoggerFactory.getLogger("VersionUtil");
    private static final Pattern versionPattern = Pattern.compile("version=([0-9]+.[0-9]+.[0-9]+)");
    private static String version = null;

    public static Version getCurrentVersion() {
        if (version == null) {
            Properties props = new Properties();
            try {
                props.load(VersionUtil.class.getClassLoader().getResourceAsStream("project.properties"));
            } catch (IOException e) {
                log.warn("Could not load project.properties");
                return null;
            }
            version = props.getProperty("version");
        }
        return Version.fromString(version);
    }


    public static Version getLatestVersion() {
        try {
            String request = HttpHelper.getTextHttpDocument("https://bitbucket.org/Scoin0/rnrpbot/raw/master/RNRPBot3/target/classes/project.properties");
            Matcher matcher = versionPattern.matcher(request);
            if (matcher.find()) {
                return Version.fromString(matcher.group(1));
            }
        } catch (HttpHelperException e) {
            e.printStackTrace();
        }
        return Version.fromString("1");
    }
}
