package rnrpbot.utils;

import net.dv8tion.jda.core.JDAInfo;
import rnrpbot.utils.version.Version;
import rnrpbot.utils.version.VersionUtil;

import java.awt.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {

    // Bot Information
    public static final String name = "RNRPBot";

    // RNRP
    //TODO: Move to another file
    public static String[] ids = {"114245200750051336","114245212087386113", "114905930700816387", "114245207784030210", "115302864582082563", "429215278208122880"};
    public static List<String> whitelistedUsers = new ArrayList<>();

    // Colors for Embed Messages
    public static final Color COLOR_RNRP = new Color(243, 90, 90);
    public static final Color COLOR_SUCCESS = new Color(132, 214, 162);
    public static final Color COLOR_ERROR = new Color(229, 9, 20);

    // Version
    public static final Version version = VersionUtil.getCurrentVersion();

    // Owner Information
    public static final String ownerID = "114245200750051336";

    public static String getLogo() {
        return "______ _   _ __________________       _   \r\n" +
                "| ___ \\ \\ | || ___ \\ ___ \\ ___ \\     | |  \r\n" +
                "| |_/ /  \\| || |_/ / |_/ / |_/ / ___ | |_ \r\n" +
                "|    /| . ` ||    /|  __/| ___ \\/ _ \\| __|\r\n" +
                "| |\\ \\| |\\  || |\\ \\| |   | |_/ / (_) | |_ \r\n" +
                "\\_| \\_\\_| \\_/\\_| \\_\\_|   \\____/ \\___/ \\__|"
                + "\nCurrent Version: " + version
                + "\nLatest Version: " + VersionUtil.getLatestVersion()
                + "\nJDA Version: " + JDAInfo.VERSION
                + "\nJVM: " + System.getProperty("java.version")
                + "\n";
    }

}
