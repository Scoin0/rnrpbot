package rnrpbot.utils;

import java.util.Random;

/**
 * Borrowed from ZP4RKER
 * https://github.com/zpdev/zlevels/blob/master/src/main/java/me/zp4rker/zlevels/util/LevelsUtil.java
 *
 * Until I write my own version.
 */

public class LevelingUtils {

    public static long xpToNextLevel(int level) {
        return 5 * (((long) Math.pow (level, 2)) + 10 * level + 20);
    }

    private static long levelsToXp(int levels) {
        long xp = 0;

        for(int level = 0; level <= levels; level++) {
            xp += xpToNextLevel(level);
        }

        return xp;
    }

    public static int xpToLevels(long totalXp) {
        boolean calc = true;
        int level = 1;

        while (calc) {
            long xp = levelsToXp(level);

            if (totalXp < xp) {
                calc = false;
            } else {
                level++;
            }
        }
        return level;
    }

    public static long remainingXp(long totalXp) {
        int level = xpToLevels(totalXp);
        long xp = levelsToXp(level);

        return totalXp - xp + xpToNextLevel(level);
    }

    public static int randomXp(int min, int max) {
        Random random = new Random();

        return random.nextInt((max - min) + 1) + min;
    }

}
