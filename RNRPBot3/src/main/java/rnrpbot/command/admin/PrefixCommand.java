package rnrpbot.command.admin;

import net.dv8tion.jda.core.Permission;
import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.database.objects.GuildSettings;

import java.sql.SQLException;

public class PrefixCommand extends Command {

    public PrefixCommand() {
        name = "prefix";
        description = "Check or change the prefix";
        usage.add("prefix");
        usage.add("prefix <prefix>");
        category = new Command.Category("Admin");
        commandPermission = CommandPermission.ADMIN;
    }

    public void onCommand(CommandEvent event) {

        GuildSettings gs = DatabaseUtils.guifromId(event.getGuild().getId());

        if (event.getArgs().length == 1 && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            gs.setPrefix(event.combineArgs());
            event.getChannel().sendMessage("The new prefix is: " + gs.getPrefix()).queue();
            event.getClient().setCustomPrefix(event.getGuild().getId(), gs.getPrefix());
            try {
                DatabaseManager.guildSettingsDao.update(gs);
            } catch (SQLException e) {
                e.getErrorCode();
            }
        } else {
            event.getChannel().sendMessage("Your prefix is set to: " + gs.getPrefix()).queue();
        }
    }
}
