package rnrpbot.command.admin;

import rnrpbot.command.Command;

import rnrpbot.command.CommandEvent;
import rnrpbot.core.ExitCode;

public class ShutdownCommand extends Command {

    public ShutdownCommand() {
        name = "shutdown";
        aliases = new String[]{"sd"};
        description = "Force shutdown the bot";
        usage.add("shutdown");
        usage.add("shutdown {reason}");
        category = new Command.Category("Admin");
    }

    @Override
    public void onCommand(CommandEvent event) {
        String reason = "No reason given";
        if (event.getArgs().length > 0 && event.getAuthor().getId().contains("114245200750051336")) {
            reason = event.combineArgs();
            event.getChannel().sendMessage("Shutting down the bot for reason: " + reason).queue();
            System.exit(ExitCode.SHUTDOWN.getExitCode());
        } else {
            event.getChannel().sendMessage("Shutting down the bot for reason: " + reason).queue();
            System.exit(ExitCode.SHUTDOWN.getExitCode());
        }
    }
}
