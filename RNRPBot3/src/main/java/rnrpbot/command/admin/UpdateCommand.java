package rnrpbot.command.admin;

import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;
import rnrpbot.utils.version.VersionUpdate;
import rnrpbot.utils.version.VersionUtil;

public class UpdateCommand extends Command {

    public UpdateCommand() {
        name = "update";
        description = "Checks for an update and updates if one is found";
        usage.add("update");
        aliases = new String[]{"u"};
        category = new Command.Category("Admin");
    }

    @Override
    public void onCommand(CommandEvent event) {

        if (!VersionUtil.getLatestVersion().compareVersion(VersionUtil.getCurrentVersion())) {
            event.getChannel().sendMessage("You are up to date.").queue();
            return;
        } else {
            event.getChannel().sendMessage("Update found! Current Version: " + VersionUtil.getCurrentVersion() + " | Latest Version: " + VersionUtil.getLatestVersion()).queue();
            VersionUpdate.checkForUpdate();
        }
    }

}
