package rnrpbot.command;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.utils.PermissionUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Predicate;

public abstract class Command {

    protected String name = "null";
    protected String description = "Description unavailable";
    protected String expandedDescription = null;
    protected ExtendedPosition exDescPos = ExtendedPosition.BEFORE;
    protected ArrayList<String> usage = new ArrayList<>();
    protected Category category = new Category("unassigned");
    protected int cooldown = 0;
    protected CooldownScope cooldownScope = CooldownScope.USER;
    protected CommandPermission commandPermission = CommandPermission.USER;
    protected CommandState commandState = CommandState.GUILD;
    protected String[] aliases = new String[0];
    protected Command[] subcommands = new Command[0];
    protected Permission[] requiredUserPermissions = new Permission[0];
    protected Permission[] requiredBotPermissions = new Permission[0];

    public abstract void onCommand (CommandEvent event) throws Throwable;

    public final void run(CommandEvent event) {

        if (event.getArgs().length > 0) {
            String[] args = event.getArgs();
            for (Command command : subcommands) {
                if (command.isCommandFor(args[0])) {
                    event.setArgs(args.length > 1 ? Arrays.copyOfRange(args, 1 , args.length) : new String[0]);
                    command.run(event);
                }
            }
        }

        if (commandPermission == CommandPermission.OWNER && !(event.isBotOwner())) {
            error(event, "Unfortunately only the bot Owner can use this command.");
            return;
        }

        if (commandPermission == CommandPermission.RNRP && !(event.isRNRP())) {
            error(event, "Unfortunately only RNRP can use this command.");
            return;
        }

        if (commandPermission == CommandPermission.ADMIN && !(event.getMember().hasPermission(Permission.ADMINISTRATOR))) {
            error(event, "Unfortunately only the admin can use this command.");
            return;
        }

        if (category != null && !category.test(event)) {
            error(event, category.getFailMessage());
            return;
        }

        if (commandState.equals(CommandState.DM) && event.isFromType(ChannelType.TEXT)) {
            error(event, "This command can not be run outside DMs.");
            return;
        } else if (event.isFromType(ChannelType.TEXT)) {
            for (Permission p : requiredBotPermissions) {
                if (p.isChannel()) {
                    if (p.name().startsWith("VOICE")) {
                        VoiceChannel vc = event.getMember().getVoiceState().getChannel();
                        if (vc == null) {
                            error(event, "No Perms.");
                            return;
                        } else if (!PermissionUtil.checkPermission(vc, event.getSelfMember(), p)) {
                            error(event, "No Perms. `" + p.getName() + "` (Voice Channel Permission)");
                            return;
                        }
                    } else {
                        if (!PermissionUtil.checkPermission(event.getTextChannel(), event.getSelfMember(), p)) {
                            error(event, "No Perms.`" + p.getName() + "` (Channel Permission)");
                            return;
                        }
                    }
                    if (!PermissionUtil.checkPermission(event.getTextChannel(), event.getSelfMember(), p)) {
                        error(event, "No Perms.`" + p.getName() + "`");
                        return;
                    }
                }
            }
            for (Permission p : requiredUserPermissions) {
                if (p.isChannel()) {
                    if (!PermissionUtil.checkPermission(event.getTextChannel(), event.getMember(), p)) {
                        error(event, "No Perms.`" + p.getName() + "` (Channel Permission)");
                        return;
                    }
                } else {
                    if (!PermissionUtil.checkPermission(event.getTextChannel(), event.getMember(), p)) {
                        error(event, "No Perms.`" + p.getName() + "` (Channel Permission)");
                        return;
                    }
                }
            }
        } else if (commandState.equals(CommandState.GUILD)) {
            error(event, "This command cannot be run inside DMs");
        }

        if (cooldown > 0) {
            String key = getCooldownKey(event);
            int remaining = event.getClient().getRemainingCooldown(key);
            if (remaining > 0) {
                String error = getCooldownError(remaining);
                if (error != null) {
                    event.getChannel().sendMessage("This command is on cooldown " + remaining + " seconds remaining.").queue();
                    return;
                }
            }
            else event.getClient().applyCooldown(key, cooldown);
        }

        try {
            onCommand(event);
        } catch (Throwable t) {
            throwException(t, event);
        }
    }

    public String getCooldownKey(CommandEvent event) {
        switch (cooldownScope) {
            case USER: return cooldownScope.genKey(name, event.getAuthor().getIdLong());
            case USER_GUILD: return event.getGuild() != null ? cooldownScope.genKey(name, event.getAuthor().getIdLong(), event.getGuild().getIdLong()) : CooldownScope.USER_CHANNEL.genKey(name, event.getAuthor().getIdLong(), event.getChannel().getIdLong());
            case USER_CHANNEL: return cooldownScope.genKey(name, event.getAuthor().getIdLong(), event.getChannel().getIdLong());
            default: return "";

        }
    }

    public String getCooldownError(int remaining) {

        if (remaining <= 0) return null;

        String front = "Sorry, that command is on cooldown for " + remaining + " more seconds.";
        if (cooldownScope.equals(CooldownScope.USER))
            return front;
        else if (cooldownScope.equals(CooldownScope.USER_GUILD))
            return front  + " " + CooldownScope.USER_CHANNEL.errorSpecification;
        else return front + " " + cooldownScope.errorSpecification;

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String[] getAliases() {
        return aliases;
    }

    public ArrayList<String> getUsage() {
        return usage;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isCommandFor(String input) {

        if (name.equalsIgnoreCase(input)) return true;

        for (String alias : aliases) {
            if (alias.equalsIgnoreCase(input))
                return true;
        }
        return false;
    }

    private void error(CommandEvent event, String message) {
        if (message != null) {
            event.getChannel().sendMessage(message).queue();
        }
    }

    protected void addUsage(String usage) {
        this.usage.add("%s" + usage);
    }

    protected boolean isMention(String mention) {
        return mention.matches("<@!?(\\d+)>");
    }

    protected void throwException (Throwable t, CommandEvent event) {
        throwException(t, event, "No Description Added");
    }

    protected void throwException (Throwable t, CommandEvent event, String description) {
        String endline = System.getProperty("line.separator");
        // some shitty stuff
    }

    public String getExpandedDescription() {
        return expandedDescription;
    }

    public ExtendedPosition getExDescriptionPosition() {
        return exDescPos;
    }

    protected enum CooldownScope {
        USER ("U:%d",""),
        USER_CHANNEL ("U:%d|C:%d", "in this channel"),
        USER_GUILD ("U:%d|G:%d", "in this server");

        private final String format;
        final String errorSpecification;

        CooldownScope(String format, String errorSpecification) {
            this.format = format;
            this.errorSpecification = errorSpecification;
        }

        String genKey(String name, long id) {
            return genKey(name, id, -1);
        }

        String genKey(String name, long id1, long id2) {
            if (id2 == -1)
                return name + "|" + String.format(format, id1);
            else return name + "|" + String.format(format, id1, id2);
        }
    }

    protected enum CommandState {

        DM, GUILD, BOTH;
        CommandState() {}

    }

    protected enum CommandPermission {

        USER, MOD, ADMIN, OWNER, RNRP;

        CommandPermission() {}

    }

    protected enum ExtendedPosition {

        BEFORE, AFTER;

        ExtendedPosition() {}

    }

    public static class Category {

        private final String name;
        private String failMessage;
        private final Predicate<CommandEvent> predicate;

        public Category(String name) {
            this.name = name;
            this.failMessage = null;
            this.predicate = null;
        }

        public Category(String name, String failMessage, Predicate<CommandEvent> predicate) {
            this.name = name;
            this.failMessage = failMessage;
            this.predicate = predicate;
        }

        public String getName() {
            return name;
        }

        public Predicate<CommandEvent> getPredicate() {
            return predicate;
        }

        public boolean test(CommandEvent event) {
            return predicate == null || predicate.test(event);
        }

        public String getFailMessage() {
            return failMessage;
        }

        @Override
        public boolean equals(Object o) {
            if(!(o instanceof Category)) {
                return false;
            }
            Category other = (Category) o;
            return Objects.equals(name, other.name) && Objects.equals(predicate, other.predicate) && Objects.equals(failMessage, other.failMessage);
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 17 * hash + Objects.hashCode(this.name);
            hash = 17 * hash + Objects.hashCode(this.failMessage);
            hash = 17 * hash + Objects.hashCode(this.predicate);
            return hash;
        }
    }

}
