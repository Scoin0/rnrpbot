package rnrpbot.command.general;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import rnrpbot.command.AbstractCommand;
import rnrpbot.core.DiscordBot;

public class PingCommand extends AbstractCommand {

    private static final String[] pingMessages = new String[]{
            ":ping_pong::white_small_square::black_small_square::black_small_square::ping_pong:",
            ":ping_pong::black_small_square::white_small_square::black_small_square::ping_pong:",
            ":ping_pong::black_small_square::black_small_square::white_small_square::ping_pong:",
            ":ping_pong::black_small_square::white_small_square::black_small_square::ping_pong:",
    };

    public PingCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "checks latency of the bot";
    }

    @Override
    public String getCommand() {
        return "ping";
    }

    @Override
    public String[] getAliases() {
        return new String[]{};
    }

    @Override
    public String onCommand(DiscordBot bot, String[] args, MessageChannel channel, User user, Message message) {
        long start = System.currentTimeMillis();
        channel.sendMessage(":outbox_tray: checking ping").submit();
        return "";
    }

}
