package rnrpbot.command.general;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;
import org.apache.commons.lang.StringUtils;
import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;
import rnrpbot.core.DiscordBot;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.objects.Accounts;
import rnrpbot.menu.EmbedPaginator;
import rnrpbot.utils.version.VersionUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LeaderboardCommand extends Command {

    public LeaderboardCommand() {
        name = "leaderboard";
        description = "Check the global leader board";
        aliases = new String[]{"lb"};
        category = new Command.Category("General");
    }

    public void onCommand(CommandEvent event) {

        EmbedPaginator.Builder builder = new EmbedPaginator.Builder();
        builder.addUsers(event.getAuthor());
        builder.setEventWaiter(DiscordBot.waiter);
        builder.setFinalAction(f -> f.clearReactions().queue());
        builder.waitOnSinglePage(true);
        builder.showPageNumbers(true);

        try {
            List<Accounts> accounts = DatabaseManager.accountDao.queryForAll();

            List<Accounts> info = new ArrayList<>();

            for(Accounts info2 : DatabaseManager.accountDao) {
                info.add(info2);
            }

            info.sort((info1, info2) -> {
                if(info1.getTotalXp() == info2.getTotalXp()) return 0;
                return info1.getTotalXp() < info2.getTotalXp() ? 1 : -1;
            });

            accounts.forEach(a -> {

                if (a.getLevel() == 1) return;

                for (int i = 0; i <= info.size(); i += 10) {

                    List<Accounts> currentPage = info.subList(i, (i + 10 < info.size() ? i + 10 : info.size()));

                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setTitle("Top Members by Level");
                    embedBuilder.setDescription(StringUtils.join(currentPage.stream().map(acc -> "Level " + acc.getLevel() + "| " + acc.getUsername()).collect(Collectors.toList()),
                            "\n"));
                    embedBuilder.setFooter("RNRPBot v" + VersionUtil.getCurrentVersion(), null);
                    builder.addItems(embedBuilder.build());

                }
            });
            builder.build().display(event.getChannel());

        } catch (SQLException e) {

        }

    }

}
