package rnrpbot.command.general;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDAInfo;
import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;
import rnrpbot.core.DiscordBot;
import rnrpbot.main.RNRPBot;
import rnrpbot.utils.Constants;
import rnrpbot.utils.version.VersionUtil;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class StatsCommand extends Command {

    private final Runtime runtime = Runtime.getRuntime();
    private final int mb = 1024 * 1024;
    private final int gb = 1024 * 1024 * 1024;
    private final DecimalFormat format = new DecimalFormat("#.##");

    public StatsCommand() {
        name = "stats";
        description = "Get some information about the bot";
        aliases = new String[]{"stat", "info", "information"};
        usage.add("stats");
        usage.add("stats expand");
        category = new Command.Category("General");
    }

    @Override
    public void onCommand(CommandEvent event) {
        long startTime = System.currentTimeMillis() - RNRPBot.startTime;
        long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        long totalMemory = runtime.totalMemory();
        long maxMemory = runtime.maxMemory();
        long totalChannels = event.getJDA().getTextChannelCache().size();
        long totalVoiceChannels = event.getJDA().getVoiceChannelCache().size();
        float usedPercent = (float) ((usedMemory * 100) / totalMemory);
        float allocatedPercent = (float) ((totalMemory * 100) / maxMemory);
        long totalGuilds = event.getJDA().getGuildCache().size();
        long totalUsers = event.getJDA().getUserCache().size();


        if (event.getArgs().length == 0) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setThumbnail(event.getSelfUser().getAvatarUrl());
            builder.setTitle("RNRPBot v" + VersionUtil.getCurrentVersion());
            builder.addField("Online for: ", convertMillisToTime(startTime), false);
            builder.addField("Version: ", VersionUtil.getCurrentVersion().toString(), true);
            builder.addField("Library: ", "JDA", true);
            builder.addField("Total Servers: ", String.valueOf(totalGuilds), false);
            builder.addField("Number of Commands", String.valueOf(event.getClient().getCommands().size()), false);
            builder.addField("Source Code: ", "https://bitbucket.org/Scoin0/rnrpbot/src/master/", false);
            builder.setColor(Constants.COLOR_RNRP);
            event.getChannel().sendMessage(builder.build()).queue();
        } else {
            if (event.getArgs()[0].equalsIgnoreCase("expand")) {
                StringBuilder builder = new StringBuilder();
                builder.append("Version: " + VersionUtil.getCurrentVersion());
                builder.append("\nLibrary: JDA");
                builder.append("\nJDA Version: " + JDAInfo.VERSION);
                builder.append("\nRAM: " + shortenMemory(usedMemory) + " / " + shortenMemory(totalMemory) + " (" + usedPercent + "% Used)");
                builder.append("\nAllocated: " + shortenMemory(totalMemory) + " / " + shortenMemory(maxMemory) + " (" + allocatedPercent + "%Used)");
                builder.append("\nOnline for: " + convertMillisToTime(startTime));

                if (DiscordBot.totalShards > 1) {
                    builder.append("\nCurrent Shard: " + (event.getJDA().getShardInfo().getShardId()) + 1);
                    builder.append("\nTotal Number of Shards " + DiscordBot.totalShards);
                    builder.append("\nChannels (In this shard): " + event.getJDA().getTextChannelCache().size());
                    builder.append("\nVoice Channels (In this shard): " + event.getJDA().getVoiceChannelCache().size());
                    builder.append("\nUsers (In this shard): " + event.getJDA().getUserCache().size());
                    builder.append("\nTotal Servers: " + totalGuilds);
                    builder.append("\nTotal Channels: " + totalChannels);
                    builder.append("\nTotal Voice Channels: " + totalVoiceChannels);
                    builder.append("\nTotal Users: " + totalUsers);
                } else {
                    builder.append("\nServers: " + event.getJDA().getGuildCache().size());
                    builder.append("\nChannels: " + event.getJDA().getTextChannelCache().size());
                    builder.append("\nVoice Channels: " + event.getJDA().getVoiceChannelCache().size());
                    builder.append("\nUsers: " + event.getJDA().getUserCache().size());
                }
                builder.append("\nTotal Commands: " + event.getClient().getCommands().size());
                event.getChannel().sendMessage("```yaml\n" + builder.toString() + "```").queue();
            }
        }
    }

    private String shortenMemory(long memory) {
        if ((memory / mb) < 1024) return (memory / mb) + "MB";
        else return Float.valueOf(format.format((float) memory / gb)) + "GB";
    }

    private String convertMillisToTime(long millis) {
        long days = TimeUnit.MILLISECONDS.toDays(millis);
        long hours = TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis));
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));

        return (days != 0 ? days + " days, " : "") + (hours != 0 ? hours + " hours, " : "") + (minutes != 0 ? minutes + " minutes, " : "") + (seconds != 0 ? seconds + " seconds. " : "");

    }
}
