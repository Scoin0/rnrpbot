package rnrpbot.command.general;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.MessageEmbed;
import org.apache.commons.lang.StringUtils;
import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;
import rnrpbot.core.DiscordBot;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.database.objects.GuildSettings;
import rnrpbot.menu.EmbedPaginator;
import rnrpbot.utils.version.VersionUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public class HelpCommand  extends Command {

    public HelpCommand() {
        name = "help";
        description = "Shows available commands, you can specify a command as well.";
        usage.add("help");
        usage.add("help {command}");
        aliases = new String[]{"commands"};
        category = new Command.Category("General");
    }

    @Override
    public void onCommand(CommandEvent event) {

        String[] args = event.getArgs();

        if (args.length == 0) {
            sendHelp(event);
        } else {
            sendCommandHelp(event, args[0]);
        }

    }

    private void sendHelp(CommandEvent event) {
        GuildSettings gs = DatabaseUtils.guifromId(event.getGuild().getId());
        List<Category> categories = event.getClient().getCommands().stream().map(Command::getCategory).distinct().sorted(Comparator.comparing(Category::getName)).collect(Collectors.toList());
        EmbedPaginator.Builder builder = new EmbedPaginator.Builder();
        builder.addUsers(event.getAuthor());
        builder.setEventWaiter(DiscordBot.waiter);
        builder.setFinalAction(m -> m.clearReactions().queue());
        builder.waitOnSinglePage(true);
        builder.showPageNumbers(true);

        categories.forEach(c -> {
            if (c.getName().equalsIgnoreCase("admin") && !event.getMember().hasPermission(Permission.ADMINISTRATOR)) return;
            if (c.getName().equalsIgnoreCase("rnrp") && !event.isRNRP()) return;
            if (c.getName().equalsIgnoreCase("owner") && !event.isBotOwner()) return;
            if (c.getName().equalsIgnoreCase("unassigned")) return;
            List<Command> commands = new ArrayList<>(event.getClient().getCommands().stream().filter(cmd -> cmd.getCategory().equals(c)).sorted(Comparator.comparing(Command::getName)).collect(Collectors.toList()));
            for (int i = 0; i < commands.size(); i += 10) {
                List<Command> currentPage = commands.subList(i, (i + 10 < commands.size() ? i + 10 : commands.size()));
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle(c.getName());
                embedBuilder.setDescription(StringUtils.join(currentPage.stream().map(cmd -> gs.getPrefix() + cmd.getName() + " - " + cmd.getDescription()).collect(Collectors.toList()), "\n"));
                embedBuilder.setFooter("RNRPBot v" + VersionUtil.getCurrentVersion(), null);
                embedBuilder.setAuthor("RNRPBot", null, event.getSelfUser().getAvatarUrl());
                builder.addItems(embedBuilder.build());
            }
        });
        builder.build().display(event.getChannel());
    }

    private void sendCommandHelp(CommandEvent event, String commandName) {


        boolean isFound;

        Optional<Command> command = event.getClient().getCommands().stream().filter(c -> c.getName().equalsIgnoreCase(commandName)).findAny();

        if (command.isPresent()) {
            if (command.get().getCategory().getName().equalsIgnoreCase("admin")) isFound = event.isBotOwner();
            else isFound = !command.get().getCategory().getName().equalsIgnoreCase("unassigned");
        } else {
            isFound = false;
        }

        if (isFound) {
            event.getChannel().sendMessage(getCommandHelp(event, command.get())).queue();
        } else {
            event.getChannel().sendMessage("Command not found.").queue();
        }

    }

    private MessageEmbed getCommandHelp(CommandEvent event, Command command) {
        GuildSettings gs = DatabaseUtils.guifromId(event.getGuild().getId());
        EmbedBuilder builder = new EmbedBuilder();
        builder.addField("Command: ", command.getName(), true);
        builder.addField("Category:",command.getCategory().getName(), true);
        builder.addField("Alises: ", StringUtils.join(command.getAliases(), ", ") + ",", false);
        builder.addField("Description: ", command.getDescription(), false);
        if (command.getExpandedDescription() != null && command.getExDescriptionPosition().equals(ExtendedPosition.BEFORE))
            builder.addField("", command.getExpandedDescription().replaceAll("\\{p}", Matcher.quoteReplacement(event.getPrefix())), false);
        builder.addField("Usage (<> Required, {} Optional):", StringUtils.join(command.getUsage().stream().map(usage -> gs.getPrefix() + usage).collect(Collectors.toList()), "\n"), false);
        if (command.getExpandedDescription() != null && command.getExDescriptionPosition().equals(ExtendedPosition.AFTER))
            builder.addField("", command.getExpandedDescription().replaceAll("\\{p}", Matcher.quoteReplacement(event.getPrefix())), false);
        builder.setFooter("RNRPBot v" + VersionUtil.getCurrentVersion(), null);
        builder.setAuthor("RNRPBot v" + VersionUtil.getCurrentVersion(), null, event.getSelfUser().getAvatarUrl());
        return builder.build();
    }

}
