package rnrpbot.command.general;

import net.dv8tion.jda.core.EmbedBuilder;
import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.database.objects.Accounts;
import rnrpbot.utils.Constants;
import rnrpbot.utils.LevelingUtils;
import rnrpbot.utils.version.VersionUtil;

public class ProfileCommand extends Command {

    public ProfileCommand() {
        name = "profile";
        description = "Check out your profile";
        aliases = new String[]{"pf"};
        usage.add("profile");
        usage.add("profile <user>");
        category = new Command.Category("General");
    }

    public void onCommand(CommandEvent event) {

        if (event.getArgs().length == 0) {
            Accounts user = DatabaseUtils.accfromId(event.getAuthor().getId());
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle(event.getAuthor().getName() + "'s Profile");
            eb.setThumbnail(event.getAuthor().getAvatarUrl());
            eb.addField("Current Level", "" + user.getLevel(), true);
            eb.addField("Total Experience", "" + user.getTotalXp(), true);
            eb.addField("Progress Till Next Levelup", getProgressBar(LevelingUtils.remainingXp(user.getTotalXp()), LevelingUtils.xpToNextLevel(user.getLevel()))
                    + " " + getPercentage(LevelingUtils.remainingXp(user.getTotalXp()), LevelingUtils.xpToNextLevel(user.getLevel())), true);
            //eb.addField("Money", "$" + user.getBalance(), true);
            eb.setColor(Constants.COLOR_SUCCESS);
            eb.setFooter("RNRPBot v" + VersionUtil.getCurrentVersion(), null);
            event.getChannel().sendMessage(eb.build()).queue();
        } else if (event.getArgs().length == 1) {
            if (event.getMentionedUsers().size() == 0) {
                event.getChannel().sendMessage("Please specify a user to lookup or just type profile").queue();
            } else if (event.getMentionedUsers().size() == 1 && event.getMentionedUsers().get(0) == event.getJDA().getSelfUser()) {
                event.getChannel().sendMessage("You cannot lookup the profile for the bot.").queue();
            } else {
                Accounts user = DatabaseUtils.accfromId(event.getMentionedUsers().get(0).getId());
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(event.getMentionedUsers().get(0).getName() + "'s Profile");
                eb.setThumbnail(event.getMentionedUsers().get(0).getAvatarUrl());
                eb.addField("Current Level", "" + user.getLevel(), true);
                eb.addField("Total Experience", "" + user.getTotalXp(), true);
                eb.addField("Progress Till Next Levelup", getProgressBar(LevelingUtils.remainingXp(user.getTotalXp()), LevelingUtils.xpToNextLevel(user.getLevel()))
                        + " " + getPercentage(LevelingUtils.remainingXp(user.getTotalXp()), LevelingUtils.xpToNextLevel(user.getLevel())), true);
                //eb.addField("Money", "$" + user.getBalance(), true);
                eb.setColor(Constants.COLOR_SUCCESS);
                eb.setFooter("RNRPBot v" + VersionUtil.getCurrentVersion(), null);
                event.getChannel().sendMessage(eb.build()).queue();
            }
        }
    }

    public String getProgressBar(long currentXP, long totalEXP) {
        StringBuilder bar = new StringBuilder();

        final String inactive = "▬";
        final String active = ":negative_squared_cross_mark:";
        final int parts = 10;

        int activeBlock = (int) (((float) currentXP / (float) totalEXP) * parts);

        for (int i = 0; i < parts; i++) {
            if (i <= activeBlock) {
                bar.append(active);
            } else {
                bar.append(inactive);
            }
        }
        return bar.toString();
    }

    public String getPercentage(long currentXP, long totalEXP) {

        int percentage = (int) ((float)(currentXP * 100) / (float) totalEXP);

        String percent = percentage + "%";

        return percent;
    }
}
