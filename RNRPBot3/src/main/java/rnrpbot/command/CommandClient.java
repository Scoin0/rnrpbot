package rnrpbot.command;


import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import rnrpbot.database.DatabaseUtils;
import rnrpbot.database.objects.GuildSettings;
import rnrpbot.utils.Constants;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CommandClient extends ListenerAdapter {

    private final OffsetDateTime start;
    private final String botOwner;

    private String prefix = "!";
    private final HashMap<String, String> customPrefixes;
    private final ArrayList<Command> commands;
    private final HashMap<String, Integer> commandIndex;
    private final HashMap<String, OffsetDateTime> cooldowns;
    private final HashMap<String, Integer> disabledUsers;
    private final HashMap<String, List<String>> disabledMembers;

    private TextChannel joinLogChannel;
    //Prize Channel, Bot Channel

    public CommandClient() {
        start = OffsetDateTime.now();
        commands = new ArrayList<>();
        customPrefixes = new HashMap<>();
        cooldowns = new HashMap<>();
        commandIndex = new HashMap<>();
        disabledUsers = new HashMap<>();
        disabledMembers = new HashMap<>();

        botOwner = Constants.ownerID;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setCustomPrefix(String guildId, String index) {
        if (customPrefixes.containsKey(guildId)) {
            if (prefix.equals(this.prefix)) {
                customPrefixes.remove(guildId);
            } else {
                customPrefixes.replace(guildId, index);
            }
        } else {
            if (prefix.equals(this.prefix)) return;
            customPrefixes.put(guildId, index);
        }
    }

    public void addCommand(Command command) {
        String name = command.getName();
        synchronized (commandIndex) {
            if (commandIndex.containsKey(name)) throw new IllegalArgumentException("The command added already has an index: " + name + "!");
            for (String alias : command.getAliases()) {
                if (commandIndex.containsKey(alias)) throw new IllegalArgumentException("The command added already has an index: " + name + "!");
                commandIndex.put(alias, commands.size());
            }
            commandIndex.put(name, commands.size());
        }
        commands.add(command);
    }

    public ArrayList<Command> getCommands() {
        return commands;
    }

    public void disabledUser(String Id, int type) {
        if (!disabledUsers.containsKey(Id)) disabledUsers.put(Id, type);
    }

    public void enableUser(String Id) {
        if (disabledUsers.containsKey(Id)) disabledUsers.remove(Id);
    }

    public void disabledMember(String guildId, String userId) {
        if (!disabledMembers.containsKey(guildId)) disabledMembers.put(guildId, new ArrayList<>());
        disabledMembers.get(guildId).add(userId);
    }

    public void enableMember(String guildId, String userId) {
        if (!disabledMembers.containsKey(guildId)) return;
        if (disabledMembers.get(guildId).contains(userId)) disabledMembers.get(guildId).remove(userId);
    }

    public OffsetDateTime getCooldown(String name) {
        return cooldowns.get(name);
    }

    public int getRemainingCooldown(String name) {
        if (cooldowns.containsKey(name)) {
            int time = (int) OffsetDateTime.now().until(cooldowns.get(name), ChronoUnit.SECONDS);
            if (time <= 0) {
                cooldowns.remove(name);
                return 0;
            }
            return time;
        }
        return 0;
    }

    public void applyCooldown(String name, int seconds) {
        cooldowns.put(name, OffsetDateTime.now().plusSeconds(seconds));
    }

    public void purgeCooldowns() {
        OffsetDateTime now = OffsetDateTime.now();
        cooldowns.keySet().stream().filter((str) -> (cooldowns.get(str).isBefore(now))).collect(Collectors.toList()).stream().forEach(str -> cooldowns.remove(str));
    }

    public String getPrefix(String guildId) {
        return customPrefixes.getOrDefault(guildId, prefix);
    }

    public boolean isUserDisabled(String userId) {
        return disabledUsers.containsKey(userId) && disabledUsers.get(userId) >= 1;
    }

    public boolean isMemberDisabled(MessageReceivedEvent event) {
        return event.isFromType(ChannelType.TEXT) && (disabledMembers.containsKey(event.getGuild().getId()));
        // ? disabledMembers.get(event.getGuild().getId()).contains(event.getAuthor().getId())
    }

    public String getBotOwner() {
        return botOwner;
    }

    @Override
    public void onReady(ReadyEvent event) {
        JDA jda = event.getJDA();
        jda.getGuilds().forEach(guild -> {
            //load in settings
        });
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        GuildSettings gs = DatabaseUtils.guifromId(event.getGuild().getId());

        if (event.getAuthor().isBot()) return; // Do not listen to bots.

        String parts[] = null;
        String rawContent = event.getMessage().getContentRaw().replace("@everyone", "@\u200Beveryone").replace("@here", "@\u200Bhere");

        if (event.isFromType(ChannelType.TEXT)) {
            if (rawContent.startsWith(event.getGuild().getSelfMember().getAsMention()))
                parts = Arrays.copyOf(rawContent.substring(rawContent.indexOf(">")+1).trim().split("\\s+", 2), 2);
        }

        if (event.isFromType(ChannelType.PRIVATE)) {
            parts = Arrays.copyOf(rawContent.split("\\s+", 2), 2);
        }

        if (parts == null && customPrefixes.containsKey(event.getGuild().getId()) && rawContent.startsWith(customPrefixes.get(event.getGuild().getId())))
            parts = Arrays.copyOf(rawContent.substring(customPrefixes.get(event.getGuild().getId()).length()).trim().split("\\s+", 2), 2);

        if (parts == null && !customPrefixes.containsKey(event.getGuild().getId()) && rawContent.startsWith(gs.getPrefix()))
            parts = Arrays.copyOf(rawContent.substring(gs.getPrefix().length()).trim().split("\\s+", 2), 2);

        if (parts != null && !isUserDisabled(event.getAuthor().getId()) && !isMemberDisabled(event)) {
            if (event.isFromType(ChannelType.PRIVATE) || event.getTextChannel().canTalk()) {
                String name = parts[0];
                String[] args = parts[1] == null ? new String[0] : parts[1].split("\\s+");

                final Command command;
                synchronized (commandIndex) {
                    int i = commandIndex.getOrDefault(name.toLowerCase(), -1);
                    command = i != -1 ? commands.get(i) : null;
                }

                if (command != null) {
                    CommandEvent commandEvent = new CommandEvent(event, args, this);
                    command.run(commandEvent);
                    return;
                }
            }
        }
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {

        if (event.getGuild().getSelfMember().getJoinDate().plusMinutes(10).isBefore(OffsetDateTime.now())) return;
        //Send Welcome Event Message
    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        // Send Leave Event Message
    }





}
