package rnrpbot.command;


import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.apache.commons.lang.StringUtils;
import rnrpbot.utils.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * From Kekbot until I write my own version. This extends to all Command, CommandEvent, and CommandClient
 */

public class CommandEvent {

    private final MessageReceivedEvent event;
    private String[] args;
    private final CommandClient client;

    public CommandEvent (MessageReceivedEvent event, String[] args, CommandClient client) {
        this.event = event;
        this.args = args;
        this.client = client;
    }

    public String[] getArgs() {
        return args;
    }

    public MessageReceivedEvent getEvent() {
        return event;
    }

    public CommandClient getClient() {
        return client;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public boolean isBotOwner() {
        return event.getAuthor().getId().equals(Constants.ownerID);
    }

    public boolean isRNRP() {
        Constants.whitelistedUsers.addAll(Arrays.asList(Constants.ids));
        return Constants.whitelistedUsers.contains(event.getAuthor().getId());
    }

    public ChannelType getChannelType() {
        return event.getChannelType();
    }

    public boolean isFromType(ChannelType type) {
        return type == event.getChannelType();
    }

    public MessageChannel getChannel() {
        return event.getChannel();
    }

    public TextChannel getTextChannel() {
        return event.getTextChannel();
    }

    public Guild getGuild() {
        return event.getGuild();
    }

    public String getPrefix() {
        return "!"; //Change later
    }

    public User getAuthor() {
        return event.getAuthor();
    }

    public Member getMember() {
        return event.getMember();
    }

    public SelfUser getSelfUser() {
        return event.getJDA().getSelfUser();
    }

    public Member getSelfMember() {
        return event.getGuild() == null ? null : event.getGuild().getSelfMember();
    }

    public JDA getJDA() {
        return event.getJDA();
    }

    public Message getMessage() {
        return event.getMessage();
    }

    public List<User> getMentionedUsers() {
        List<User> mentionedUsers = new ArrayList<>(event.getMessage().getMentionedUsers());
        if (event.getMessage().getContentRaw().startsWith(event.getGuild().getSelfMember().getAsMention())) {
            mentionedUsers.remove(0);
        }
        return mentionedUsers;
    }

    public List<Member> getMentionedMembers() {
        List<Member> mentionedMembers = new ArrayList<>(event.getMessage().getMentionedMembers());
        if (event.getMessage().getContentRaw().startsWith(event.getGuild().getSelfMember().getAsMention())) {
            mentionedMembers.remove(0);
        }
        return mentionedMembers;
    }

    public String combineArgs() {
        return combineArgs(0, args.length);
    }

    public String combineArgs(int start) {
        return combineArgs(start, args.length);
    }

    public String combineArgs(int start, int end) {
        if (end > args.length) throw new IllegalArgumentException("End value specified is longer than the arguments provided.");
        return StringUtils.join(Arrays.copyOfRange(args, start, end), " ");
    }

    public boolean isDisabled() {
        return false; //Fix later Add user
    }

}
