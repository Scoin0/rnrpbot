package rnrpbot.command.rnrp;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.entities.VoiceChannel;
import rnrpbot.command.Command;
import rnrpbot.command.CommandEvent;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BroomCommand extends Command {

    String[] ids = {"143625670344441856", "114245200750051336","114245212087386113", "114905930700816387", "114245207784030210", "115302864582082563"};
    List<String> whitelistedUsers = new ArrayList<>();

    public BroomCommand() {
        name = "broom";
        description = "Broomed ya!";
        expandedDescription = "Only members of RNRP can use this command.";
        usage.add("broom <user>");
        usage.add("broom <multiple users>");
        category = new Command.Category("RNRP");
        cooldown = 60;
        cooldownScope = CooldownScope.USER;
        commandPermission = CommandPermission.RNRP;
    }

    @Override
    public void onCommand(CommandEvent event) {
        whitelistedUsers.addAll(Arrays.asList(ids));
        List<VoiceChannel> vc = event.getGuild().getVoiceChannels();
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(new Color(137, 38, 245));
        builder.setTitle("YOU'VE BEEN BROOMED");
        builder.setImage("https://i.imgur.com/dUSaCPp.png");


        if (event.getArgs().length == 0) {
            event.getChannel().sendMessage("Please specify a user to broom.").queue();
            return;
        }

        if (event.getArgs().length >= 1) {
            if (event.getMentionedUsers().size() == 0 && whitelistedUsers.contains(event.getAuthor().getId())) {
                event.getChannel().sendMessage("Please mention a user to broom").queue();
            } else if (event.getMentionedUsers().size() == 1) {
                if (event.getMentionedUsers().get(0) == event.getJDA().getSelfUser()) {
                    event.getChannel().sendMessage("You cannot broom the bot. What are you crazy?").queue();
                } else if (event.getMentionedUsers().get(0) == event.getAuthor()) {
                    event.getChannel().sendMessage("You cannot broom yourself.").queue();
                }
             } else if (event.getMentionedUsers().size() >= 1); {
                for (int i = 0; i < event.getMentionedUsers().size(); i++) {
                    User user = event.getMentionedUsers().get(i);
                    if (whitelistedUsers.contains(user.getId())) {
                        int randomVC = new Random().nextInt(vc.size());
                        event.getGuild().getController().moveVoiceMember(event.getMentionedMembers().get(i), vc.get(randomVC)).queue();
                        event.getChannel().sendMessage(builder.build()).queue();
                    }
                }
                event.getChannel().deleteMessageById(event.getMessage().getId()).complete();
            }
        } else {
            event.getChannel().sendMessage("You are not authorized to use this command.").queue();
        }
    }
}
