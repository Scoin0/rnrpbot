package rnrpbot.core;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.command.CommandClient;
import rnrpbot.command.admin.PrefixCommand;
import rnrpbot.command.general.LeaderboardCommand;
import rnrpbot.command.general.ProfileCommand;
import rnrpbot.command.owner.ShutdownCommand;
import rnrpbot.command.owner.UpdateCommand;
import rnrpbot.command.general.HelpCommand;
import rnrpbot.command.general.StatsCommand;
import rnrpbot.command.rnrp.BroomCommand;
import rnrpbot.configuration.BotConfig;
import rnrpbot.event.BotEvents;
import rnrpbot.main.RNRPBot;
import rnrpbot.utils.version.VersionTask;

import javax.security.auth.login.LoginException;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class DiscordBot {

    static final Logger log = LoggerFactory.getLogger("DiscordBot");

    public static final EventWaiter waiter = new EventWaiter(Executors.newSingleThreadScheduledExecutor(), false);

    public final long startTimeStamp;

    private static AtomicReference<JDA> jda;

    private int shardId;

    public static int totalShards;

    private volatile boolean isReady = false;

    private DiscordContainer container;

    private static CommandClient client = new CommandClient();

    public DiscordBot(int shardId, int totalShards, DiscordContainer container) {

        jda = new AtomicReference<>();
        this.shardId = shardId;
        this.totalShards = totalShards;
        setContainer(container);
        startTimeStamp = System.currentTimeMillis() / 1000L;


        while (true) {
            try {
                restartJDA();
                break;
            } catch (LoginException | InterruptedException | RateLimitedException e) {
                try {
                    Thread.sleep(5000L);
                } catch (InterruptedException e1) {
                    e.printStackTrace();
                }
            }
        }
        markAsReady();
    }

    public void restartJDA() throws LoginException, InterruptedException, RateLimitedException {
        JDABuilder builder = new JDABuilder(AccountType.BOT).setToken(BotConfig.getBotToken());

        if (totalShards > 1 ) {
            builder.useSharding(shardId, totalShards);
        }
        builder.setBulkDeleteSplittingEnabled(false);
        builder.setEnableShutdownHook(false);
        //EventManager
        log.info("Starting shard: " + shardId);
        jda.set(builder.buildBlocking());
        jda.get().getPresence().setGame(Game.watching(" you sleep ;)"));
        jda.get().addEventListener(client, new BotEvents(), waiter);
        log.info("Shard: " + shardId + " has started!");
        addCommands();
    }

    public void updateJDA(JDA jda) {
        this.jda.compareAndSet(this.jda.get(), jda);
    }

    public static JDA getJDA() {
        return jda.get();
    }

    public int getShardId() {
        return shardId;
    }

    public boolean isReady() {
        return isReady;
    }

    public void markAsReady() {
        if (isReady) {
            return;
        }

        isReady = true;
        container.allShardsReady();
    }

    public DiscordContainer getContainer() {
        return container;
    }

    public void setContainer(DiscordContainer container) {
        this.container = container;
    }

    public static CommandClient getCommandClient() {
        return client;
    }

    public void initOnce() {
    }

    public void addCommands() {
        client.addCommand(new StatsCommand());
        client.addCommand(new UpdateCommand());
        client.addCommand(new BroomCommand());
        client.addCommand(new ShutdownCommand());
        client.addCommand(new HelpCommand());
        client.addCommand(new PrefixCommand());
        client.addCommand(new LeaderboardCommand());
        client.addCommand(new ProfileCommand());
    }

}
