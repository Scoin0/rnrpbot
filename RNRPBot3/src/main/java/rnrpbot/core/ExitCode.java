package rnrpbot.core;

public enum ExitCode {

    REBOOT(100),
    SHUTDOWN(101),
    ADDSHARD(102),
    UPDATE(200),
    UNKNOWN(-1);

    private final int exitCode;

    ExitCode(int exitCode) {
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        return exitCode;
    }

}
