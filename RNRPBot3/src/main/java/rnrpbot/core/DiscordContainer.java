package rnrpbot.core;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.configuration.BotConfig;

import javax.security.auth.login.LoginException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class DiscordContainer {

    static final Logger log = LoggerFactory.getLogger("DiscordContainer");

    private final int numberOfShards;
    private final DiscordBot[] shards;
    private final AtomicBoolean statusLocked = new AtomicBoolean(false);
    private final AtomicInteger numberOfGuilds;
    private final ScheduledExecutorService scheduler;
    private volatile boolean allShardsReady = false;
    private volatile boolean terminationRequested = false;
    private volatile ExitCode rebootReason = ExitCode.UNKNOWN;

    public DiscordContainer(int numberOfGuilds) throws LoginException, InterruptedException, RateLimitedException {
        scheduler = Executors.newScheduledThreadPool(3);
        this.numberOfGuilds = new AtomicInteger(numberOfGuilds);
        this.numberOfShards = getRecommendedShards();
        shards = new DiscordBot[numberOfShards];
        //Handlers
        initShards();
    }

    public synchronized boolean tryRestartingShard(int shardId) {
        try {
            restartShard(shardId);
        } catch (InterruptedException | LoginException | RateLimitedException e) {
            log.error("Restarting shard encountered an error ");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void schedule(Runnable task, Long delay, TimeUnit timeunit) {
        scheduler.schedule(task, delay, timeunit);
    }

    public synchronized void restartShard(int shardId) throws InterruptedException, LoginException, RateLimitedException {

        for (Guild guild : shards[shardId].getJDA().getGuilds()) {
            //
        }

        log.info("Shutting down shard " + shardId);
        shards[shardId].getJDA().shutdown();
        log.info("Shutdown shard " + shardId);

        schedule(() -> {
            while (true) {
                try {
                    shards[shardId].restartJDA();
                    break;
                } catch (LoginException | InterruptedException | RateLimitedException e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(5000L);
                    } catch (InterruptedException ignored) {

                    }
                }
            }
        },5L, TimeUnit.SECONDS);

    }

    public synchronized void requestExit(ExitCode reason) {
        if (!terminationRequested) {
            terminationRequested = true;
            rebootReason = reason;
        }
    }

    public void guildJoined() {
        int suggestedShards = 1 + ((numberOfGuilds.incrementAndGet() + 500) / 2000);
        if (suggestedShards > numberOfShards) {
            terminationRequested = true;
            rebootReason = ExitCode.ADDSHARD;
        }
    }

    public int getRecommendedShards() {
        try {
            HttpResponse<JsonNode> request = Unirest.get("https://discordapp.com/api/gateway/bot")
                    .header("Authorization", "Bot " + BotConfig.getBotToken())
                    .header("Content-Type", "application/json")
                    .asJson();
            return Integer.parseInt(request.getBody().getObject().get("shards").toString());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public void guildLeft() {
        numberOfGuilds.decrementAndGet();
    }

    public DiscordBot[] getShards() {
        return shards;
    }

    public DiscordBot getShardFor(String discordGuildId) {
        if (numberOfShards == 1) {
            return shards[0];
        }
        return getShardFor(Long.parseLong(discordGuildId));
    }

    public DiscordBot getShardFor(Long discordGuildId) {
        if (numberOfShards == 1) {
            return shards[0];
        }
        return shards[calcShardId(discordGuildId)];
    }

    public int calcShardId(long discordGuildId) {
        return (int) ((discordGuildId >> 22) % numberOfShards);
    }

    private void initShards() throws LoginException, InterruptedException, RateLimitedException {
        for (int i = 0; i < shards.length; i++) {
            log.info("Starting shard #{} of {}", i, shards.length);
            shards[i] = new DiscordBot(i, shards.length, this);
            if (i == 0) {
                shards[i].initOnce();
            }
            Thread.sleep(5000L);
        }

        for (DiscordBot shard: shards) {
            //setLastActon
        }
    }

    public boolean allShardsReady() {
        if (allShardsReady) {
            return allShardsReady;
        }

        for (DiscordBot shard : shards) {
            if (shard == null || !shard.isReady()) {
                return false;
            }
        }

        allShardsReady = true;
        return true;
    }

    public boolean isTerminationRequested() {
        return terminationRequested;
    }

    public ExitCode getRebootReason() {
        return rebootReason;
    }

    public boolean isStatusLocked() {
        return statusLocked.get();
    }

    public void setStatusLocked(boolean locked) {
        statusLocked.set(locked);
    }
}
