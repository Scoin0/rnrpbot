package rnrpbot.main;

import net.dv8tion.jda.core.JDA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rnrpbot.configuration.BotConfig;
import rnrpbot.core.DiscordBot;
import rnrpbot.core.DiscordContainer;
import rnrpbot.core.ExitCode;
import rnrpbot.database.DatabaseManager;
import rnrpbot.database.controllers.GuildController;
import rnrpbot.utils.Constants;
import rnrpbot.utils.version.VersionTask;

public class RNRPBot{

    static final Logger log = LoggerFactory.getLogger("RNRPBot");
    public static final long startTime = System.currentTimeMillis();

    private static DiscordContainer botContainer = null;

    private volatile ExitCode rebootReason = ExitCode.UNKNOWN;

    public static JDA jda;

    public static void main(String[] args) throws Exception {
        System.out.println(Constants.getLogo());
        BotConfig.initConfiguration();
        DatabaseManager.loadDatabase();

        if (BotConfig.getBotToken() == null) {
            log.warn("You've failed to insert the bot token in the configuration. Please do that.");
            System.exit(ExitCode.SHUTDOWN.getExitCode());
        } else {
            try {
                botContainer = new DiscordContainer((GuildController.getActiveGuildCount()));

                if (BotConfig.getUseUpdater() == true) {
                    VersionTask.updateScheduler();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
